// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, 
// а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

function sendRequest(url, method = 'GET', options)  {
    return fetch(url, {method: method, ...options}).then((response) =>
    response.json()
    );
}

const films = document.querySelector('.films');

const API = "https://ajax.test-danit.com/api/swapi/films";


function findFilms (element) {
    sendRequest(`${API}`).then((data) => {
        console.log(data);

        data.forEach((el) => {
            let {episodeId, characters, name, openingCrawl} = el;
            let actors = characters;
                 
            element.insertAdjacentHTML('beforeend', `
            <ul id="films${episodeId}" >
            <li class = 'episodeId'> ${ episodeId }</li>
            <li  class = 'name'> ${ name }</div>         
            <li  class = 'openingCrawl'> ${ openingCrawl }</li>
            </ul>
`)

              actors.forEach((people) => {
                sendRequest(`${people}`).then((data) => {
              const film = document.getElementById(`films${episodeId}`);
                const character = data.name;
                
                film.insertAdjacentHTML('beforeend', `<span>${character}</span>
                `)
                  
                })
            })
     
  

           
        })
    })
}


findFilms (films)

